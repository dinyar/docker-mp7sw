mp7sw docker image
============

The docker image is built and pushed to the repository with

```
docker build -t mp7sw:v2_4_1_noPatterns_directConnection . && docker tag mp7sw:v2_4_1_noPatterns_directConnection gitlab-registry.cern.ch/cms-cactus/firmware/ugmt/mp7sw:v2_4_1_noPatterns_directConnection && docker push gitlab-registry.cern.ch/cms-cactus/firmware/ugmt/mp7sw:v2_4_1_noPatterns_directConnection
```

