# Use CC7 as base
FROM cern/cc7-base:latest

# Installing IPbus
COPY ipbus-sw.centos7.x86_64.repo /etc/yum.repos.d/ipbus-sw.repo
RUN yum groupinstall uhal -y
ENV LD_LIBRARY_PATH "$LD_LIBRARY_PATH:/opt/cactus/lib"

# Adding MP7SW 
RUN yum install make gcc-c++ boost-devel pugixml-devel python-devel -y
COPY mp7sw* /mp7sw
WORKDIR /mp7sw/mp7
RUN make 

# Install ssh and git
RUN yum install openssh-clients sshpass git -y

# Install Python3
RUN yum install python3 python3-devel -y

# Move into final workdir
WORKDIR /ugmt_tests
COPY connections.xml .
